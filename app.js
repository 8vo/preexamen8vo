import path from "path";
import express from 'express';
import json from 'body-parser';
import { fileURLToPath } from "url";
import rutas from './router/index.routes.js'
const port = 8080;

const _filename = fileURLToPath(import.meta.url);
const _dirname = path.dirname(_filename);

const app = express();

app.set("view engine", "ejs");
app.use(json.urlencoded({extended: true}));

app.use(express.static(path.join(_dirname, 'public')));

app.use(rutas.router);

app.listen(port, () => {
    console.log("Server corriendo en el puerto: "+port);
})
